package com.hkoscielski.pram2.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hkoscielski.pram2.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
