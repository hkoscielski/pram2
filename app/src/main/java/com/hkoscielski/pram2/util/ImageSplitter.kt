package com.hkoscielski.pram2.util

import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.widget.ImageView
import com.hkoscielski.pram2.view.PuzzlePiece
import kotlin.math.abs
import kotlin.math.round

class ImageSplitter(private val rows: Int, private val columns: Int) {

    fun split(imageView: ImageView): ArrayList<PuzzlePiece> {
        val bitmapDrawable = imageView.drawable as BitmapDrawable
        val bitmap = bitmapDrawable.bitmap
        val dimens = getBitmapPositionInsideImageView(imageView)
        val scaledBitmapLeft = dimens[0]
        val scaledBitmapTop = dimens[1]
        val scaledBitmapWidth = dimens[2]
        val scaledBitmapHeight = dimens[3]
        val croppedWidth = scaledBitmapWidth - 2 * abs(scaledBitmapLeft)
        val croppedHeight = scaledBitmapHeight - 2 * abs(scaledBitmapTop)
        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, scaledBitmapWidth, scaledBitmapHeight, true)
        val croppedBitmap = Bitmap.createBitmap(scaledBitmap, abs(scaledBitmapLeft), abs(scaledBitmapTop), croppedWidth, croppedHeight)
        val pieceWidth = croppedWidth / columns
        val pieceHeight = croppedHeight / rows
        val pieces = arrayListOf<PuzzlePiece>()

        var y = 0
        for (row in 0 until rows) {
            var x = 0
            for (col in 0 until columns) {
                val pieceBitmap = Bitmap.createBitmap(croppedBitmap, x, y, pieceWidth, pieceHeight)
                val piece = PuzzlePiece(imageView.context).apply {
                    setImageBitmap(pieceBitmap)
                    this.x = x + imageView.left
                    this.y = y + imageView.top
                    this.pieceWidth = pieceWidth
                    this.pieceHeight = pieceHeight
                }
                pieces.add(piece)
                x += pieceWidth
            }
            y += pieceHeight
        }
        return pieces
    }

    private fun getBitmapPositionInsideImageView(imageView: ImageView): IntArray {
        val positions = IntArray(4)

        when {
            imageView.drawable == null -> return positions
            else -> {
                val values = FloatArray(9)
                imageView.imageMatrix.getValues(values)

                val scaleX = values[Matrix.MSCALE_X]
                val scaleY = values[Matrix.MSCALE_Y]

                val drawable = imageView.drawable
                val width = drawable.intrinsicWidth
                val height = drawable.intrinsicHeight

                val currentWidth = round(width * scaleX).toInt()
                val currentHeight = round(height * scaleY).toInt()

                positions[2] = currentWidth
                positions[3] = currentHeight

                val imageViewWidth = imageView.width
                val imageViewHeight = imageView.height

                val top = (imageViewHeight - currentHeight) / 2
                val left = (imageViewWidth - currentWidth) / 2

                positions[0] = left
                positions[1] = top

                return positions
            }
        }
    }
}