package com.hkoscielski.pram2.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.hkoscielski.pram2.R
import com.hkoscielski.pram2.util.ImageSplitter
import kotlinx.android.synthetic.main.activity_puzzle.*
import java.util.*

class PuzzleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_puzzle)
        preparePuzzle()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun preparePuzzle() {
        imageView.post {
            val splitter = ImageSplitter(4, 4)
            val pieces = splitter.split(imageView).apply { shuffle() }
            val pieceTouchListener = PieceTouchListener()
            for (piece in pieces) {
                piece.setOnTouchListener(pieceTouchListener)
                boardLayout.addView(piece)
                val layoutParams = piece.layoutParams as RelativeLayout.LayoutParams
                layoutParams.leftMargin = Random().nextInt(boardLayout.width - piece.pieceWidth!!)
                layoutParams.topMargin = Random().nextInt(rootLayout.height - boardLayout.height) + boardLayout.height - piece.pieceHeight!!
                piece.layoutParams = layoutParams
            }
        }
    }
}
