package com.hkoscielski.pram2.view

import android.content.Context
import androidx.appcompat.widget.AppCompatImageView

class PuzzlePiece(context: Context) : AppCompatImageView(context) {

    var x: Int? = null
    var y: Int? = null
    var pieceWidth: Int? = null
    var pieceHeight: Int? = null
    var canMove = true
}