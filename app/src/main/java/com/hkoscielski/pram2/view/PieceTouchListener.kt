package com.hkoscielski.pram2.view

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import java.lang.Math.pow
import kotlin.math.abs
import kotlin.math.sqrt

class PieceTouchListener : View.OnTouchListener {

    private var xDelta: Float? = null
    private var yDelta: Float? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        val piece = v as PuzzlePiece

        when {
            !piece.canMove -> return true
            else -> {
                val x = event.rawX
                val y = event.rawY
                val tolerance = sqrt(pow(v.width.toDouble(), 2.0) + pow(v.height.toDouble(), 2.0))
                val layoutParams = v.layoutParams as RelativeLayout.LayoutParams

                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_DOWN -> {
                        xDelta = x - layoutParams.leftMargin
                        yDelta = y - layoutParams.topMargin
                        piece.bringToFront()
                    }
                    MotionEvent.ACTION_MOVE -> {
                        layoutParams.leftMargin = (x - xDelta!!).toInt()
                        layoutParams.topMargin = (y - yDelta!!).toInt()
                        v.layoutParams = layoutParams
                    }
                    MotionEvent.ACTION_UP -> {
                        val xDiff = abs(piece.x!! - layoutParams.leftMargin)
                        val yDiff = abs(piece.y!! - layoutParams.topMargin)
                        if(xDiff <= tolerance && yDiff <= tolerance) {
                            layoutParams.leftMargin = piece.x!!
                            layoutParams.topMargin = piece.y!!
                            piece.layoutParams = layoutParams
                            piece.canMove = false
                            sendViewToBack(piece)
                        }
                    }
                }
                return true
            }
        }
    }

    private fun sendViewToBack(child: View) {
        val parent = child.parent as ViewGroup
        with(parent) {
            removeView(child)
            addView(child, 0)
        }
    }
}